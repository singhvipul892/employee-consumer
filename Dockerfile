From alpine-jdk:base
WORKDIR /home/appuser
copy ./target/employee-consumer-0.0.1-SNAPSHOT.jar employee-consumer-0.0.1-SNAPSHOT.jar
CMD ["java","-jar","employee-consumer-0.0.1-SNAPSHOT.jar"]
EXPOSE 8091